% Facts
male(jack).
male(oliver).
male(ali).
male(james).
male(simon).
male(harry).
female(helen).
female(sophie).
female(jess).
female(lily).
female(sarah).

parent_of(jack,jess). % jack is the parent of jess
parent_of(jack,lily).
parent_of(jack, sarah).
parent_of(helen, jess).
parent_of(helen, lily).
parent_of(oliver,james).
parent_of(sophie, james).
parent_of(jess, simon).
parent_of(ali, simon).
parent_of(lily, harry).
parent_of(james, harry).

father_of(X, Y) :- male(X), parent_of(X, Y).
mother_of(X, Y) :- female(X), parent_of(X, Y).
grandfather_of(X, Y) :- male(X), parent_of(X, Z), parent_of(Z, Y).
grandmother_of(X, Y) :- female(X), parent_of(X, Z), parent_of(Z, Y).
sister_of(X, Y) :- female(X), parent_of(F, X), parent_of(F, Y), X \= Y.
aunt_of(X, Y) :- female(X), sister_of(X, Z), parent_of(Z, Y).
ancestor_of(X, Y) :- parent_of(X, Y).
ancestor_of(X, Y) :- parent_of(X, Z), ancestor_of(Z, Y).
