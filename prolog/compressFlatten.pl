%compresses list to remove letters that repeat directle after eachother
compress([X], [X]).
compress([H1, H2 | T], X) :- H1 = H2, compress([H2 | T], X); H1 \= H2, compress([H2|T], Y), X = [H1 | Y].

my_flatten([], []).
my_flatten([H | T], R) :- is_list(H), my_flatten(H, X1), my_flatten(T, X2), append(X1, X2, R).
my_flatten([H | T], R) :- not(is_list(H)), my_flatten(T, X1), R = [H | X1].
