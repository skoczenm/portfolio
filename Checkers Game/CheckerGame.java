// Kenna Skoczen Copyright(2020)

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

//CheckerGame method that works with GUI
public class CheckerGame extends JFrame implements MouseListener, ActionListener{
  
   //fields
   private boolean turnPink, captureMove, mustJump;
   private CheckerBoard cb;
   private JLabel statusLabel;
   private int picks;
   private CheckerPiece tbm, dest;
   private static JOptionPane aboutPane, rulesPane;
  
   private static char[][] boardStatus = new char[][] {
       {'e', 'b', 'e', 'b', 'e', 'b', 'e', 'b'},
       {'b', 'e', 'b', 'e', 'b', 'e', 'b', 'e'},
       {'e', 'b', 'e', 'b', 'e', 'b', 'e', 'b'},
       {'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e'},
       {'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e'},
       {'r', 'e', 'r', 'e', 'r', 'e', 'r', 'e'},
       {'e', 'r', 'e', 'r', 'e', 'r', 'e', 'r'},
       {'r', 'e', 'r', 'e', 'r', 'e', 'r', 'e'},
   };
  
   //default constructor
   public CheckerGame() {
       //initializes fields and components
       picks = 0;
       turnPink = true; mustJump = false;
       setSize(515, 595);
       cb = new CheckerBoard(boardStatus);
       addMouseEvent(cb);
       JMenuBar menuBar = new JMenuBar();
       JPanel statusPanel = new JPanel(new GridLayout(2,1));
       CheckerGame.aboutPane = new JOptionPane("About");

      
       //set up the status bar
       statusLabel = new JLabel("New game! Pink starts first.", JLabel.CENTER);
       JLabel informationLabel = new JLabel("This game was developed by Kenna Skoczen.", JLabel.CENTER);
       statusPanel.add(statusLabel);
       statusPanel.add(informationLabel);
      
       //game menu
       JMenu gameMenu = new JMenu("Game");
       JMenuItem newItem = new JMenuItem("New");
       newItem.addActionListener(this);
       JMenuItem exitItem = new JMenuItem("Exit");
       exitItem.addActionListener(this);
       gameMenu.add(newItem);
       gameMenu.add(exitItem);
      
      
       //help menu
       JMenu helpMenu = new JMenu("Help");
       JMenuItem rulesItem = new JMenuItem("Checker Game Rules");
       rulesItem.addActionListener(this);
       JMenuItem aboutItem = new JMenuItem("About Checker Game App");
       aboutItem.addActionListener(this);
       helpMenu.add(rulesItem);
       helpMenu.add(aboutItem);
  
       menuBar.add(gameMenu);
       menuBar.add(helpMenu);
      
       //add components to the frame
       setJMenuBar(menuBar);
       add(cb, BorderLayout.CENTER);
       add(statusPanel, BorderLayout.SOUTH);
       setDefaultCloseOperation(EXIT_ON_CLOSE);
       setVisible(true);  
   }

   //main method
   public static void main(String[] args) {
       CheckerGame cg = new CheckerGame();
   }

   //gets status label
   public JLabel getStatusLabel() {
       return statusLabel;
   }

   //sets checker to be moved
   public void setToBeMoved(CheckerPiece toBeMoved) {
       if (toBeMoved == null) {
           statusLabel.setText("You haven't picked a checker. Please pick a pink one.");
           return;
       }
       switch (toBeMoved.validToBeMoved(turnPink)) {
       case 2: statusLabel.setText("No checker to be moved. Please pick another one"); break;
       case 3: statusLabel.setText("It's pink's turn. Please pick a pink checker."); break;
       case 4: statusLabel.setText("It's yellow's turn. Please pick a yellow checker."); break;
       case 1: this.tbm = toBeMoved; picks = 1;
           if (turnPink) statusLabel.setText("A pink checker was picked");
           else statusLabel.setText("A yellow checker was picked");
       }
   }

   //sets destination if valid, if not shows error message
   public void setDestination(CheckerPiece dest) {
       if (dest == null) {
           statusLabel.setText("You haven't picked a checker. Please pick a pink one.");
           return;
       }
       switch (dest.validDestination(tbm, cb.getBoardStatus())){
       case 2: statusLabel.setText("The square is not empty. Please make another move"); picks = 0; break;
       case 3: statusLabel.setText("Can only move diagonal. Please make another move"); picks = 0; break;
       case 4: statusLabel.setText("Cannot move horizontally. Please make another move"); picks = 0; break;
       case 5: statusLabel.setText("Cannot move backward. Please make another move"); picks = 0; break;
       case 6: statusLabel.setText("Cannot move more than one space. Please make another move"); picks = 0; break;
       case 1:
           this.dest = dest; picks = 2;
           if (dest.isACapture(tbm, cb.getBoardStatus())) captureMove = true;
       }
   }
  
   //captures checker between checker being moved and destination square
   private void capture(CheckerPiece first, CheckerPiece second) {
       if (first.getStatus() == 'r' || first.getStatus() == 'q') cb.setPink(cb.getPink() - 1);
       if (first.getStatus() == 'b' || first.getStatus() == 'k') cb.setYellow(cb.getYellow() - 1);
       //Removes the captured checker
       cb.setCheckerPiece((first.getRow() + second.getRow())/2,
               (first.getCol() + second.getCol())/2,'e');
   }

   //adds mouseEvent to checkerpiece
   private void addMouseEvent(CheckerBoard cb) {
       for (int i = 0; i < cb.getBoardStatus().length; i++) {
           for (int j = 0; j < cb.getBoardStatus().length; j++)
               ((CheckerPiece) cb.getComponent(i * 8 + j)).addMouseListener(this);
          
       }
   }
  
   public void mouseClicked(MouseEvent e) {
       CheckerPiece cp = (CheckerPiece) e.getComponent();
       statusLabel.setText(cb.toString());
       if (picks == 0) setToBeMoved(cp);
       else if (picks == 1) setDestination(cp);
       if (picks == 2 && tbm != null && dest != null) {
           if (mustJump() && !captureMove) {
               statusLabel.setText("You must make a jump if you can! Please make a jump move");
               resetFields();
           }  
           else {
               moveChecker(tbm, dest); resetFields();
               if (turnPink) statusLabel.setText(cb.toString() + "[Pink turn]");
               else statusLabel.setText(cb.toString() + "[Yellow turn]");
           }
       }
       if (cb.notMoveable()) endGame();

   }
  
   //moves checker and captures if capturing
   private void moveChecker(CheckerPiece first, CheckerPiece second) {
       //Captures if it's capturing
       if (captureMove)
           capture(first, second);
       //Moves the checker to the new destination
       cb.setCheckerPiece(second.getRow(), second.getCol(), first.getStatus());
       cb.setCheckerPiece(first.getRow(), first.getCol(),'e');
       cb.setCheckersState();
   }
  
   //displays winner and disables all moves
   private void endGame() {
       if (turnPink) statusLabel.setText("Yellow won!");
       else statusLabel.setText("Pink won!");
       cb.setEnabled(false);
   }

   public void actionPerformed(ActionEvent e) {
       if (e.getActionCommand().equals("New")) reset();
       if (e.getActionCommand().equals("Exit")) dispose();
       if (e.getActionCommand().equals("Checker Game Rules"))
           JOptionPane.showMessageDialog(this, "For more information, use the link: "+
       "#https://www.wikihow.com/Play-Checkers", "Rules", JOptionPane.INFORMATION_MESSAGE);
       if (e.getActionCommand().equals("About Checker Game App"))
           JOptionPane.showMessageDialog(this, "Kenna Skoczen, skoczemm@miamioh.edu, Miami University",
                   "About", JOptionPane.INFORMATION_MESSAGE);
   }
  
   //resets fields to original state
   private void resetFields() {
       if ((captureMove && dest.isCapturable()) || (dest.isKing() && dest.isCapturable())) {
           picks = 1; tbm = dest; dest = null;
       }
       else {
           if (!mustJump || captureMove) turnPink = !turnPink;
           picks = 0; tbm = null; dest = null;  
       }
       mustJump = mustJump();
       captureMove = false;
   }
  
   //checks if capture move can be made
   private boolean mustJump() {
       if (turnPink && cb.getPinkCanCapture() != 0) return true;
       if (!turnPink && cb.getYellowCanCapture() != 0) return true;
       return false;
   }
  
   //resets checkerboard
   private void reset() {
       cb.reset(boardStatus);
       statusLabel.setText("New Game! Pink starts first.");
       mustJump = false; captureMove = false; tbm = null; dest = null; picks = 0; turnPink = true;
   }
  
   
   public void mousePressed(MouseEvent e) {}

  
   public void mouseReleased(MouseEvent e) {}

   
   public void mouseEntered(MouseEvent e) {}

   
   public void mouseExited(MouseEvent e) {}
}
