// Kenna Skoczen Copyright(2020)

public class IllegalCheckerboardArgumentException extends Exception{
    public IllegalCheckerboardArgumentException(){
        super();
    }
    
    public IllegalCheckerboardArgumentException(String message){
        super(message);
    }
}
