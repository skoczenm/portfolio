// Kenna Skoczen Copyright(2020)

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComponent;

//represents a square on the board and draws a square to represent appropriate status
public class CheckerPiece extends JComponent {
  
   //fields
   private char status;
   private int row, col;
   private final int SIDE = 60, RADIUS = 40;
   private boolean capturable, moveable;
  
  //constructs checkers using input
   public CheckerPiece(int row, int col, char status) {
       if (row < 0 || row > 7)
        try { 
            throw new IllegalCheckerboardArgumentException();
        } catch (IllegalCheckerboardArgumentException e) {
            System.out.println("row value must be between 0 and 7.");
        }
       if (col < 0 || col > 7)
        try {
            throw new IllegalCheckerboardArgumentException();
        } catch (IllegalCheckerboardArgumentException e) {
            System.out.println("column value must be between 0 and 7.");
        }
       if (status != 'r' && status != 'b' && status != 'e' && status != 'k' && status != 'q')
        try {
            throw new IllegalCheckerboardArgumentException();
        } catch (IllegalCheckerboardArgumentException e) {
            System.out.println("checker status invalid.");
        }
       if ((col + row) % 2 == 0 && status != 'e')
        try {
            throw new IllegalCheckerboardArgumentException();
        } catch (IllegalCheckerboardArgumentException e) {
            System.out.println("checker status invalid.");
        }
       if ((col + row) % 2 == 1 && status == 'e' && (row < 3 || row > 4))
        try {
            throw new IllegalCheckerboardArgumentException();
        } catch (IllegalCheckerboardArgumentException e) {
            System.out.println("checker status invalid.");
        }
       this.row = row;
       this.col = col;
       this.status = status;
   }
  
   //method to override paintComponent
   @Override
   public void paintComponent(Graphics g) {
       if ((col + row) % 2 == 0) g.setColor(Color.WHITE);
       else g.setColor(Color.BLACK);
       g.fillRect(0, 0, SIDE, SIDE);
       g.setColor(Color.BLACK);
       g.drawRect(0, 0, SIDE, SIDE);
       if (status == 'e') return;
       if (status == 'b' || status == 'k') g.setColor(Color.PINK);
       if (status == 'r' || status == 'q') g.setColor(Color.YELLOW);
       g.fillOval(10, 10, RADIUS, RADIUS);
       g.setColor(Color.PINK);
       if (this.isKing()) g.setColor(Color.YELLOW);
       g.drawOval(10, 10, RADIUS, RADIUS);
   }

   //returns status of checker square
   public char getStatus() {
       return status;
   }

   //gets status of checker square
   public void setStatus(char status) {
       this.status = status;
       repaint();
   }

   //gets row of checker square
   public int getRow() {
       return row;
   }

   //gets the column of checker square
   public int getCol() {
       return col;
   }

   //gets capturable field of checker set
   public boolean isCapturable() {
       return capturable;
   }

   //sets capturable field of checker set
   public void setCapturable(boolean capturable) {
       this.capturable = capturable;
   }

   //gets moveable field of checkerpiece
   public boolean isMoveable() {
       return moveable;
   }

   //sets moveable field of checkerpiece
   public void setMoveable(boolean moveable) {
       this.moveable = moveable;
   }

   //checks to see if a checker is king
   public boolean isKing() {
       if (status == 'b' & row == 7) {
           status = 'k';
           return true;
       }
       if (status == 'r' & row == 0) {
           status = 'q';
           return true;
       }
       if (status == 'k' || status == 'q') return true;
       return false;
   }
  
   //checks to see if the piece being moved is valid
   public int validToBeMoved (boolean turnPink) {
       if (status == 'e') return 2;
       if (turnPink && (status == 'r' || status == 'q')) return 3;
       if (!turnPink && (status == 'b' || status == 'k')) return 4;
       return 1;
   }
  
   //makes sure checkerpiece object is a valid destination
   public int validDestination (CheckerPiece tbm, char[][] boardStatus) {
       if (status != 'e') return 2;
       if ((row + col) % 2 == 0) return 3;
       if (tbm.getRow() == this.row) return 4;
       if (!tbm.isKing() && !isForward(tbm)) return 5;
       if (!isACapture(tbm, boardStatus) && !isValidMove(tbm)) return 6;
       return 1;
   }
  
   //checks to see if the current checker is jumping another
   public boolean isACapture(CheckerPiece tbm, char[][] boardStatus) {
       if (Math.abs(row - tbm.getRow()) != 2) return false;
       if (Math.abs(col - tbm.getCol()) != 2) return false;
       char mid = boardStatus[(tbm.getRow() + row)/2][(tbm.getCol() + col)/2];
       if (mid == status || mid == 'e') return false;
       return true;
   }
  
   //makes sure move being made is valid
   private boolean isValidMove(CheckerPiece tbm) {
       if (Math.abs(tbm.getRow() - row) != 1) return false;
       if (Math.abs(tbm.getCol() - col) != 1) return false;
       return true;
   }

   //method to make sure the checkerpiece is moving forawrd
   public boolean isForward(CheckerPiece tbm) {
       if (tbm.getStatus() == 'b' && row > tbm.getRow()) return true;
       if (tbm.getStatus() == 'r' && row < tbm.getRow()) return true;
       return false;
      
   }

   @Override
   public String toString() {
       return "CheckerPiece [status=" + status + ", row=" + row + ", col=" + col + ", capturable=" + capturable
               + ", moveable=" + moveable + "]";
   }
  
}
