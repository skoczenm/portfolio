// Kenna Skoczen Copyright(2020)

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Arrays;
import javax.swing.JButton;
import javax.swing.JPanel;

//extends JPanel and draws checkerboard by drawing individual square
public class CheckerBoard extends JPanel{

   //fields
   private int pink, yellow, pinkCanCapture, yellowCanCapture;
   private char[][] boardStatus;

  
  
   /*
    * constructs checkerboard using inputs from an 8x8 2D array that shows status of 
    * squares by row and column
    */
   public CheckerBoard(char[][] boardStatus) {
       pink = 12; yellow = 12;
       this.boardStatus = new char[8][8];
       setLayout(new GridLayout(8, 8));
       for(int i = 0; i < boardStatus.length; i++) {
           for (int j = 0; j < boardStatus[i].length; j++) {
               this.boardStatus[i][j] = boardStatus[i][j];
               add(new CheckerPiece(i, j, boardStatus[i][j]));
           }
       }
       setCheckersState();
   }
  
   //sets boardStatus
   public void setBoardStatus(char[][] boardStatus) {
       this.boardStatus = boardStatus;
   }
  
   //sets status of checker square
   public void setCheckerPiece(int row, int col, char status) {
       boardStatus[row][col] = status;
       ((CheckerPiece) this.getComponent(row * 8 + col)).setStatus(status);
   }

   //gets number of pink checkers that can make a capture move
   public int getPinkCanCapture() {
       return pinkCanCapture;
   }

   //gets number of yellow checkers that can make a capture move
   public int getYellowCanCapture() {
       return yellowCanCapture;
   }

   //gets number of pink checkers on board
   public int getPink() {
       return pink;
   }

   //sets number of pink checkers on board
   public void setPink(int pink) {
       this.pink = pink;
   }

   //gets number of yellow checkers on board
   public int getYellow() {
       return yellow;
   }

   //sets number of yellow checkers on board
   public void setYellow(int yellow) {
       this.yellow = yellow;
   }
  
   //gets boardStatus
   public char[][] getBoardStatus() {
       return boardStatus;
   }
  
   //gets checkerpiece object by checking square
   public CheckerPiece getCheckerPiece(int row, int col) {
       return ((CheckerPiece) this.getComponent(row * 8 + col));
   }
  
   //resets the entire checkerboard
   public void reset(char[][] boardStatus) {
       pink = 12; yellow = 12;
       for(int i = 0; i < boardStatus.length; i++) {
           for (int j = 0; j < boardStatus[i].length; j++) {
               //Changes the status of the square if it's not the same as the beginning
               if (boardStatus[i][j] != this.boardStatus[i][j])
                   setCheckerPiece(i, j, boardStatus[i][j]);
           }
       }
       setCheckersState();
   }
  
   //checks if moves can be made
   public boolean notMoveable() {
       if (pink == 0) return true;
       if (yellow == 0) return true;
       int yellowMoveable = 0, pinkMoveable = 0;
       for (Component c : getComponents()) {
           CheckerPiece cp = (CheckerPiece) c;
           if (cp.isMoveable()) {
               if (cp.getStatus() == 'b' || cp.getStatus() == 'k') pinkMoveable++;
               if (cp.getStatus() == 'r' || cp.getStatus() == 'q') yellowMoveable++;
           }
       }
       return yellowMoveable == 0 || pinkMoveable == 0;
   }
  
   //sets captureable and moveable fields of checkerpiece
   public void setCheckersState(){
       pinkCanCapture = 0; yellowCanCapture = 0;
       for (Component c : getComponents()) {
           CheckerPiece cp = (CheckerPiece) c;
           cp.setCapturable(captureable(cp.getRow(), cp.getCol()));
           cp.setMoveable(moveable(cp.getRow(), cp.getCol()));
           if ((cp.getStatus() == 'b' || cp.getStatus() == 'k') && cp.isCapturable()) pinkCanCapture++;
           if ((cp.getStatus() == 'r' || cp.getStatus() == 'q') && cp.isCapturable()) yellowCanCapture++;
       }
   }
  
  //checks if a pink checker is moveable
   private boolean pinkMoveable(int row, int col) {
       if (pinkCaptureable(row, col)) return true;
       if (row + 1 <= 7) {
           if (col - 1 >= 0 && boardStatus[row+1][col-1] == 'e') return true;
           if (col + 1 <= 7 && boardStatus[row+1][col+1] == 'e') return true;
       }
       return false;
   }
  
   //checks if yellow checker is moveable
   private boolean yellowMoveable(int row, int col) {
       if (yellowCaptureable(row, col)) return true;
       if (row - 1 >= 0) {
           if (col - 1 >= 0 && boardStatus[row-1][col-1] == 'e') return true;
           if (col + 1 <= 7 && boardStatus[row-1][col+1] == 'e') return true;
       }
       return false;
   }
  
  //checks if yellow checker can capture
   private boolean yellowCaptureable(int row, int col) {
       if (row - 2 >= 0) {
           if (col - 2 >= 0 && boardStatus[row-2][col-2] == 'e' && boardStatus[row-1][col-1] == 'b') {
               yellowCanCapture++;
               return true;
           }
           if (col + 2 <= 7 && boardStatus[row-2][col+2] == 'e' && boardStatus[row-1][col+1] == 'b') {
               yellowCanCapture++;
               return true;
           }
       }
       return false;
   }
  
   //checks if pink checker can capture
   private boolean pinkCaptureable(int row, int col) {
       if (row + 2 <= 7) {
           if (col - 2 >= 0 && boardStatus[row+2][col-2] == 'e' && boardStatus[row+1][col-1] == 'r') return true;
           if (col + 2 <= 7 && boardStatus[row+2][col+2] == 'e' && boardStatus[row+1][col+1] == 'r') return true;
       }
       return false;
   }
  
   //checks if king checker can make a capture move
   private boolean kingCaptureable(int row, int col) {
       return pinkCaptureable(row, col) || yellowCaptureable(row, col);
   }

   //checks if king checker is moveable
   private boolean kingMoveable(int row, int col) {
       return pinkMoveable(row, col) || yellowMoveable(row, col);
   }   
  
   //checkers if checker can capture
   boolean captureable(int row, int col) {
       if (boardStatus[row][col] == 'b') return pinkCaptureable(row, col);
       if (boardStatus[row][col] == 'r') return yellowCaptureable(row, col);
       if (boardStatus[row][col] == 'k' || boardStatus[row][col] == 'q')
           return kingCaptureable(row, col);
       return false;
   }
  
   //checks if checker is moveable
   public boolean moveable(int row, int col) {
       if (boardStatus[row][col] == 'b') return pinkMoveable(row, col);
       if (boardStatus[row][col] == 'r') return yellowMoveable(row, col);
       if (boardStatus[row][col] == 'k' || boardStatus[row][col] == 'q')
           return kingMoveable(row, col);
       return false;
   }

  
   @Override
   public String toString() {
       return "Pink: " + pink + ", Yellow: " + yellow + " ";
   }
}
